" modes
let VIMODE = $VIMODE
let VIDEVEL = $VIDEVEL

so ~/.config/nvim/core.vim

call plug#begin('~/.vim/plugged/')

Plug 'dracula/vim'
so ~/.config/nvim/plugin_core.vim

if VIDEVEL == '1'
    so ~/.config/nvim/plugin_devel.vim
    so ~/.config/nvim/plugin_devel_conf.vim
endif

call plug#end()

so ~/.config/nvim/plugin_core_conf.vim


" startup
if &diff
""    au VimEnter * set wrap
""    au VimEnter * wincmd p
""    au VimEnter * set wrap

    let g:signify_disable_by_default = 1

elseif VIMODE == 'BIG'
    " tagbar
    let g:tagbar_left = 0
    let g:tagbar_autopreview = 1
    let g:tagbar_compact = 1
    let g:tagbar_width = 30
    autocmd BufWinEnter * if &previewwindow | setlocal nonumber | endif

    " disable autosave
    let g:auto_save = 0

    """""""""""""
    " startup   "
    """""""""""""
    au VimEnter * TagbarToggle
    "au VimEnter * lopen
    "au VimEnter * wincmd k

    " shortcuts
    noremap <Leader>l 3:wincmd l<CR>:wincmd l<CR>
    noremap <Leader>h :wincmd k<CR>:wincmd q<CR>2:wincmd<CR>

elseif VIMODE == 'multi'
    set nu
    set foldmethod=indent

    au VimEnter * NERDTreeTabsOpen
    autocmd VimEnter * wincmd K
    autocmd VimEnter * wincmd j
    autocmd VimEnter * wincmd H
    autocmd VimEnter * vertical resize +70

elseif VIMODE == 'tiny'
    set nospell
    let g:nerdtree_tabs_open_on_console_startup = 0
    set foldmethod=manual

endif


let VITERM = $VITERM
if VITERM == '1'
    " open terminal
    au VimEnter * tabnew
    au VimEnter * terminal
    au VimEnter * vsplit
    au VimEnter * terminal git-sh
    au VimEnter * tabp
endif


" color
set t_ut=
hi Visual term=reverse cterm=reverse guibg=Grey

let g:dracula_inverse = 0
let g:dracula_colorterm = 0

colorscheme dracula



" function
so ~/.config/nvim/function.vim

" project specific configuration
"if filereadable(".vimrc")
""    so .vimrc
"endif
