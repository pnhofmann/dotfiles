" terminal fix
tnoremap <Esc> <C-\><C-n>

" Basic setup
let mapleader = ","
syntax enable
nmap <Leader>r :checktime<CR>
set encoding=utf-8
filetype plugin indent on
set smartindent
set scrolloff=3
set shiftround
set display=lastline
set viminfo='5000,f1
set writeany
set showmode
"set cursorline
set wrap
set linebreak

" basic shourcuts
nmap <silent> <Leader>A :qa!<CR>
cmap w!! w !sudo tee >/dev/null %
nmap <silent> <Leader>c :close<CR>
nmap <Leader>/ :noh<CR>

" clipboard
set clipboard=unnamed
nmap <Leader>p :set paste<CR>
nmap <Leader>P :set nopaste<CR>

" tab
set list
set listchars=tab:>-

nmap <Leader><Tab> :set smartindent tabstop=4 shiftwidth=4 expandtab softtabstop=4<CR>
nmap <Leader><S-Tab> :set nosmartindent noexpandtab softtabstop=4 tabstop=4 shiftwidth=4<CR>

" registers
nnoremap <silent> "" :registers "0123456789abcdefghijklmnopqrstuvwxyz*+.<CR>

" search
" set nowrapscan
set ignorecase
set smartcase
set incsearch
set gdefault

" Highlight fixes
hi StatusLine cterm=reverse gui=reverse ctermfg=14 ctermbg=8 guifg=#93a1a1 guibg=#002732
hi StatusLineNC cterm=reverse gui=reverse ctermfg=11 ctermbg=0 guifg=#657b83 guibg=#073642
hi User1 ctermfg=14 ctermbg=0 guifg=#93a1a1 guibg=#073642

if &compatible
    set nocompatible
endif

