" basic
Plug 'ntpeters/vim-better-whitespace'
Plug 'itchyny/lightline.vim'
Plug 'sheerun/vim-polyglot'

" search
Plug 'haya14busa/incsearch.vim'
Plug 'vim-scripts/SearchComplete'

" quotes, parenthesis, brackets, etc
Plug 'Raimondi/delimitMate'
Plug 'tpope/vim-surround'

" snippets / clipboard
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'NLKNguyen/copy-cut-paste.vim'
Plug 'vim-scripts/YankRing.vim'

" file management / tags
"Plug 'jeetsukumaran/vim-buffergator'
Plug 'scrooloose/nerdtree'
Plug 'jistr/vim-nerdtree-tabs'
Plug 'Xuyuanp/nerdtree-git-plugin'
"Plug 'vim-scripts/L9'       " FuzzyFinder requirement
"Plug 'vim-scripts/FuzzyFinder'

" version + autosave
Plug 'mbbill/undotree'
Plug '907th/vim-auto-save'
Plug 'mhinz/vim-signify'    "  git

" Navigation
Plug 'majutsushi/tagbar'
Plug 'Yggdroot/indentLine'
Plug 'troydm/zoomwintab.vim'
Plug 'matze/vim-move'

" preview
Plug 'JamshedVesuna/vim-markdown-preview'
Plug 'severin-lemaignan/vim-minimap'

" web
" Plug 'vim-scripts/browser.vim'
" Plug 'andreshazard/vim-logreview'

" ascii art
Plug 'vim-scripts/DrawIt'

" other
"
"Plug 'vim-utils/vim-man.git'
