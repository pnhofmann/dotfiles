" window

" nerd tree configuration
let g:nerdtree_tabs_autofind = 1
let g:nerdtree_tabs_focus_on_files = 1
let g:nerdtree_tabs_autofind = 0
let g:nerdtree_tabs_open_on_console_startup = 1
let g:NERDTreeWinPos = "left"

" swap, autosave, undo, reload
au CursorHold,CursorHoldI * checktime
au FocusGained,BufEnter * :checktime

map <LEADER>U :UndotreeToggle<CR>

set noswapfile
set autoread
set undofile
set undodir=~/.vimswp/

let g:auto_save = 1
let g:auto_save_events = ["InsertLeave"]

" improved search
map /  :mark s<CR><Plug>(incsearch-forward)
map ?  <Plug>(incsearch-backward)
map g/ <Plug>(incsearch-stay)

" spell
set spell
set spelllang=en

nmap <Leader>len :set spelllang=de<CR>
nmap <Leader>lde :set spelllang=en<CR>

" snippet
let g:UltiSnipsEditSplit="horizontal"
let g:UltiSnipsSnippetDirectories = ['/home/phil/.vim/snippets', 'UltiSnips']
let g:UltiSnipsSnippetsDir = "/home/phil/.vim/snippets"
nmap <silent> <Leader>u :UltiSnipsEdit<CR>
let g:deoplete#enable_at_startup = 1
inoremap <silent><expr><TAB> pumvisible() ? "\<C-n>" : "\<TAB>"
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"

" build

set foldlevel=99

nnoremap <silent> "" :registers "0123456789abcdefghijklmnopqrstuvwxyz*+.<CR>

" move
nmap <silent> <A-k> :wincmd k<CR>
nmap <silent> <A-j> :wincmd j<CR>
nmap <silent> <A-h> :wincmd h<CR>
nmap <silent> <A-l> :wincmd l<CR>

" commenter
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1
" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1
" Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDDefaultAlign = 'left'
" Set a language to use its alternate delimiters by default
let g:NERDAltDelims_java = 1
" Add your own custom formats or override the defaults
let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } }
" Allow commenting and inverting empty lines (useful when commenting a region)
let g:NERDCommentEmptyLines = 1
" Enable trimming of trailing whitespace when uncommenting
let g:NERDTrimTrailingWhitespace = 1


" move
let g:move_key_modifier = 'C'

let g:copy_cut_paste_no_mappings = 1
" Use your keymap
nmap <Leader>y <Plug>CCP_CopyLine
vmap <Leader>y <Plug>CCP_CopyText
nmap <Leader>p <Plug>CCP_PasteText
