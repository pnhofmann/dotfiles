" programming
" Language Server
"let g:ale_completion_enabled = 1

" Language server
Plug 'neoclide/coc.nvim', {'do': { -> coc#util#install()}}
Plug 'Shougo/denite.nvim'

Plug 'neoclide/jsonc.vim'

Plug 'Shougo/vimproc.vim', {'do' : 'make'}
Plug 'idanarye/vim-vebugger'


" Java
Plug 'rustushki/JavaImp.vim'

" comments
Plug 'scrooloose/nerdcommenter'

" build
Plug 'tpope/vim-dispatch'

"Plug 'maximbaz/lightline-ale'
"Plug 'w0rp/ale'
