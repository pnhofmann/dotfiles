#!/bin/sh

# Terminate already running bar instances
killall -q polybar
sleep 1
pkill -9 polybar

# Launch bar1 and bar2
if xrandr | grep 'eDP-1' | grep -q '+'; then
	polybar lap &
fi
if xrandr | grep 'HDMI-1' | grep -q '+'; then
	polybar prim &
	polybar status &
fi
if xrandr | grep 'HDMI-2' | grep -q '+'; then
	polybar sec &
fi

echo "Bars launched..."
