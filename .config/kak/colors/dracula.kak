evaluate-commands %sh{

black=rgb:282a36
gray=rgb:44475a
white=rgb:f8f8f2
blue=rgb:6272a4
cyan=rgb:8be9fd
green=rgb:50fa7b
orange=rgb:ffb86c
pink=rgb:ff79c6
purple=rgb:bd93f9
red=rgb:ff5555
yellow=rgb:f1fa8c

echo "
# Code
face global value $green
face global type $purple
face global variable $red
face global module $red
face global function $red
face global string $yellow
face global keyword $cyan
face global operator $orange
face global attribute $pink
face global comment $blue+i
face global meta $red
face global builtin $white+b

# Markup
face global title $red
face global header $orange
face global bold $pink
face global italic $purple
face global mono $green
face global block $cyan
face global link $green
face global bullet $green
face global list $white

# Built-in
face global Default $white,$black
face global PrimarySelection $black,$pink
face global SecondarySelection $black,$purple
face global PrimaryCursor $black,$cyan
face global SecondaryCursor $black,$orange
face global PrimaryCursorEol $black,$cyan
face global SecondaryCursorEol $black,$orange
face global LineNumbers $gray,$black
face global LineNumberCursor $white,$gray+b
face global LineNumbersWrapped $gray,$black+i
face global MenuForeground $blue,$white+b
face global MenuBackground $white,$blue
face global MenuInfo $cyan,$blue
face global Information $yellow,$gray
face global Error $black,$red
face global StatusLine $white,$black
face global StatusLineMode $black,$green
face global StatusLineInfo $purple,$black
face global StatusLineValue $orange,$black
face global StatusCursor $white,$blue
face global Prompt $black,$green
face global MatchingChar $black,$blue
face global BufferPadding $gray,$black
face global Whitespace $gray,$black
"

}
