# Sway config file

# MONITOR CONFIG
#output eDP-1 scale 2
#output HDMI-A-1 scale 2

set $M_1 HDMI-A-1
set $M_2 HDMI-A-2
set $M_3 eDP-1

set $K_1 u
set $K_2 i
set $K_3 o

#output $M_1 pos 0    0 res 3840x2160
#output $M_2 pos 3072 0 res 1920x1080
#output $M_3 pos 4992 0 res 3200x1800

# VARIABLES
set $mod Mod1

# directions vi style
set $left h
set $down j
set $up k
set $right l

# default application
set $term kitty
set $menu dmenu_run
set $status py3status
set $lock swaylock -i ~/.config/sway/lock.jpg -c 000000

set $firefox b
set $keepassxc shift+b

set $calibre n
set $music shift+m
set $claws m

set $telegram t


# application starter
bindsym $mod+Return exec $term

bindsym $mod+$firefox workspace firefox; exec --no-startup-id ~/.config/sway/appl 'Mozilla Firefox' firefox
bindsym $mod+$keepassxc workspace keepassxc; exec --no-startup-id ~/.config/sway/appl 'KeePassXC' keepassxc
bindsym $mod+$claws workspace claws; exec --no-startup-id ~/.config/sway/appl 'Claws Mail' claws-mail
bindsym $mod+$music workspace music
bindsym $mod+$calibre workspace calibre; exec --no-startup-id ~/.config/sway/appl 'calibre' calibre
bindsym $mod+$telegram workspace telegram; exec --no-startup-id ~/.config/sway/appl 'Telegram' telegram

# Basic shortcut
bindsym $mod+Shift+q kill
bindsym $mod+d exec $menu

bindsym $mod+f fullscreen

bindsym $mod+Shift+r reload
bindsym $mod+Shift+p exit
bindsym $mod+Shift+e exec $lock

# Moving around:
bindsym $mod+$left focus left
bindsym $mod+$down focus down
bindsym $mod+$up focus up
bindsym $mod+$right focus right

bindsym $mod+Shift+$left move left
bindsym $mod+Shift+$down move down
bindsym $mod+Shift+$up move up
bindsym $mod+Shift+$right move right

# monitor move
bindsym $mod+$K_1 focus output $M_1
bindsym $mod+shift+$K_1 move workspace to output $M_1; focus output $M_2; focus output $M_1

bindsym $mod+$K_2 focus output $M_2
bindsym $mod+shift+$K_2 move workspace to output $M_2; focus output $M_1; focus output $M_2

bindsym $mod+$K_3 focus output $M_3
bindsym $mod+shift+$K_3 move workspace to output $M_3; focus output $M_2; focus output $M_3

# workspace switch
bindsym $mod+1 workspace 1
bindsym $mod+2 workspace 2
bindsym $mod+3 workspace 3
bindsym $mod+4 workspace 4
bindsym $mod+5 workspace 5
bindsym $mod+6 workspace 6
bindsym $mod+7 workspace 7
bindsym $mod+8 workspace 8
bindsym $mod+9 workspace 9
bindsym $mod+0 workspace 10

bindsym $mod+Shift+1 move container to workspace 1
bindsym $mod+Shift+2 move container to workspace 2
bindsym $mod+Shift+3 move container to workspace 3
bindsym $mod+Shift+4 move container to workspace 4
bindsym $mod+Shift+5 move container to workspace 5
bindsym $mod+Shift+6 move container to workspace 6
bindsym $mod+Shift+7 move container to workspace 7
bindsym $mod+Shift+8 move container to workspace 8
bindsym $mod+Shift+9 move container to workspace 9
bindsym $mod+Shift+0 move container to workspace 10

# layout
bindsym $mod+shift+z splith
bindsym $mod+z splitv

bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

bindsym $mod+Shift+space floating toggle
bindsym $mod+space focus mode_toggle
bindsym $mod+a focus parent

mode "resize" {
    bindsym $left resize shrink width 10 px or 10 ppt
    bindsym $down resize grow height 10 px or 10 ppt
    bindsym $up resize shrink height 10 px or 10 ppt
    bindsym $right resize grow width 10 px or 10 ppt

    # return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+r mode "resize"


### Wallpaper
output * bg ~/.config/sway/wallpaper.jpg fill


# Tomorrow Night Eighties colors
set $background #2d2d2d
set $current    #393939
set $selection  #515151
set $foreground #cccccc
set $comment    #999999
set $red        #f2777a
set $orange     #f99157
set $yellow     #ffcc66
set $green      #99cc99
set $aqua       #66cccc
set $blue       #6699cc
set $purple     #cc99cc
set $black      #000000


# set some nice colors      title_border   title_back  title font  ???      border
client.focused              $yellow        $yellow     $selection  #000000  $yellow
client.unfocused            $yellow        $background $foreground #000000  $current
client.focused_inactive     $yellow        $yellow     $black      #000000  $yellow
client.urgent               $red           $current    $foreground #000000  $red
default_border              pixel 3

# Status Bar:
bar {
    status_command $status
    position top
     colors {
        background #2d2d2d
        statusline #cccccc
        separator #515151
	#                  border      background   text
        focused_workspace  $background $yellow      $selection
        active_workspace   $background $yellow      $black
        inactive_workspace $current    $background  $foreground
        urgent_workspace   $red        $current     $foreground
    }
}

# font
font pango:Hack 14px

# Hack: fix size
exec --no-startup-id swaymsg reload

exec --no-startup-id xfce4-terminal
