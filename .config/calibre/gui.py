# preferences for the calibre GUI

### Begin group: DEFAULT
 
# send to storage card by default
# Send file to storage card instead of main memory by default
send_to_storage_card_by_default = False
 
# confirm delete
# Confirm before deleting
confirm_delete = False
 
# main window geometry
# Main window geometry
main_window_geometry = cPickle.loads('\x80\x02cPyQt5.sip\n_unpickle_type\nq\x01U\x0cPyQt5.QtCoreq\x02U\nQByteArrayUB\x01\xd9\xd0\xcb\x00\x03\x00\x00\x00\x00\x00\x01\x00\x00\x00\x15\x00\x00\x07\x7f\x00\x00\x047\x00\x00\x00\x02\x00\x00\x00\x16\x00\x00\x07~\x00\x00\x046\x00\x00\x00\x00\x00\x00\x00\x00\x07\x80\x00\x00\x00\x02\x00\x00\x00\x16\x00\x00\x07~\x00\x00\x046\x85\x87Rq\x03.')
 
# new version notification
# Notify when a new version is available
new_version_notification = False
 
# use roman numerals for series number
# Use Roman numerals for series number
use_roman_numerals_for_series_number = True
 
# sort tags by
# Sort tags list by name, popularity, or rating
sort_tags_by = 'name'
 
# match tags type
# Match tags by any or all.
match_tags_type = 'any'
 
# cover flow queue length
# Number of covers to show in the cover browsing mode
cover_flow_queue_length = 6
 
# LRF conversion defaults
# Defaults for conversion to LRF
LRF_conversion_defaults = cPickle.loads('\x80\x02]q\x01.')
 
# LRF ebook viewer options
# Options for the LRF e-book viewer
LRF_ebook_viewer_options = None
 
# internally viewed formats
# Formats that are viewed using the internal viewer
internally_viewed_formats = cPickle.loads('\x80\x02]q\x01(X\x03\x00\x00\x00AZWq\x02X\x04\x00\x00\x00AZW3q\x03X\x04\x00\x00\x00EPUBq\x04X\x03\x00\x00\x00FB2q\x05X\x04\x00\x00\x00HTMLq\x06X\x05\x00\x00\x00HTMLZq\x07X\x05\x00\x00\x00KEPUBq\x08X\x03\x00\x00\x00LITq\tX\x03\x00\x00\x00LRFq\nX\x04\x00\x00\x00MOBIq\x0bX\x03\x00\x00\x00PDBq\x0cX\x04\x00\x00\x00POBIq\rX\x03\x00\x00\x00PRCq\x0eX\x02\x00\x00\x00RBq\x0fX\x03\x00\x00\x00SNBq\x10e.')
 
# column map
# Columns to be displayed in the book list
column_map = cPickle.loads('\x80\x02]q\x01(U\x05titleq\x02U\x08ondeviceq\x03U\x07authorsq\x04U\x04sizeq\x05U\ttimestampq\x06U\x06ratingq\x07U\tpublisherq\x08U\x04tagsq\tU\x06seriesq\nU\x07pubdateq\x0be.')
 
# autolaunch server
# Automatically launch Content server on application startup
autolaunch_server = True
 
# oldest news
# Oldest news kept in database
oldest_news = 60
 
# systray icon
# Show system tray icon
systray_icon = True
 
# upload news to device
# Upload downloaded news to device
upload_news_to_device = True
 
# delete news from library on upload
# Delete news books from library after uploading to device
delete_news_from_library_on_upload = False
 
# separate cover flow
# Show the cover flow in a separate window instead of in the main calibre window
separate_cover_flow = False
 
# disable tray notification
# Disable notifications from the system tray icon
disable_tray_notification = False
 
# default send to device action
# Default action to perform when the "Send to device" button is clicked
default_send_to_device_action = 'DeviceAction:main::False:False'
 
# asked library thing password
# Asked library thing password at least once.
asked_library_thing_password = False
 
# search as you type
# Start searching as you type. If this is disabled then search will only take place when the Enter or Return key is pressed.
search_as_you_type = False
 
# highlight search matches
# When searching, show all books with search results highlighted instead of showing only the matches. You can use the N or F3 keys to go to the next match.
highlight_search_matches = False
 
# save to disk template history
# Previously used Save to Disk templates
save_to_disk_template_history = cPickle.loads('\x80\x02]q\x01(X\x11\x00\x00\x00{#folder}/{title}q\x02X\x11\x00\x00\x00{$folder}/{title}q\x03X\x07\x00\x00\x00{title}q\x04X)\x00\x00\x00{author_sort}/{title}/{title} - {authors}q\x05X\x1f\x00\x00\x00{#folder}/{#folder_sub}/{title}q\x06e.')
 
# send to device template history
# Previously used Send to Device templates
send_to_device_template_history = cPickle.loads('\x80\x02]q\x01.')
 
# main search history
# Search history for the main GUI
main_search_history = cPickle.loads('\x80\x02]q\x01(X\x04\x00\x00\x00eureq\x02X\x05\x00\x00\x00grandq\x03X\x04\x00\x00\x00whatq\x04X\x04\x00\x00\x00sachq\x05X\x06\x00\x00\x00seelenq\x06X\n\x00\x00\x00chroniclesq\x07X\t\x00\x00\x00tag:=Bookq\x08X\x04\x00\x00\x00Bookq\tX\x06\x00\x00\x00PLACESq\nX\x08\x00\x00\x00Computerq\x0bX\x05\x00\x00\x00comicq\x0cX\t\x00\x00\x00assassinsq\rX\x05\x00\x00\x00AUDIOq\x0eX\x0f\x00\x00\x00Practical Cyberq\x0fX\t\x00\x00\x00Practicalq\x10X\x04\x00\x00\x00Nmapq\x11X\x04\x00\x00\x00nmapq\x12X\x0c\x00\x00\x00game physicsq\x13X\x0e\x00\x00\x00mastering sfmlq\x14X\x07\x00\x00\x00pyhsicsq\x15X\x08\x00\x00\x00computerq\x16X\x07\x00\x00\x00hackingq\x17X\t\x00\x00\x00no starchq\x18X\x06\x00\x00\x00Dockerq\x19X\t\x00\x00\x00No Starchq\x1ae.')
 
# viewer search history
# Search history for the e-book viewer
viewer_search_history = cPickle.loads('\x80\x02]q\x01.')
 
# viewer toc search history
# Search history for the ToC in the e-book viewer
viewer_toc_search_history = cPickle.loads('\x80\x02]q\x01.')
 
# lrf viewer search history
# Search history for the LRF viewer
lrf_viewer_search_history = cPickle.loads('\x80\x02]q\x01.')
 
# scheduler search history
# Search history for the recipe scheduler
scheduler_search_history = cPickle.loads('\x80\x02]q\x01.')
 
# plugin search history
# Search history for the plugin preferences
plugin_search_history = cPickle.loads('\x80\x02]q\x01(X\x08\x00\x00\x00wirelessq\x02X\x06\x00\x00\x00remoteq\x03X\x11\x00\x00\x00calibre companionq\x04X\t\x00\x00\x00interfaceq\x05X\x06\x00\x00\x00deviceq\x06X\x0e\x00\x00\x00send to deviceq\x07X\r\x00\x00\x00library splitq\x08e.')
 
# shortcuts search history
# Search history for the keyboard preferences
shortcuts_search_history = cPickle.loads('\x80\x02]q\x01X\x06\x00\x00\x00searchq\x02a.')
 
# jobs search history
# Search history for the tweaks preferences
jobs_search_history = cPickle.loads('\x80\x02]q\x01.')
 
# tweaks search history
# Search history for tweaks
tweaks_search_history = cPickle.loads('\x80\x02]q\x01.')
 
# worker limit
# Maximum number of simultaneous conversion/news download jobs. This number is twice the actual value for historical reasons.
worker_limit = 18
 
# get social metadata
# Download social metadata (tags/rating/etc.)
get_social_metadata = True
 
# overwrite author title metadata
# Overwrite author and title with new metadata
overwrite_author_title_metadata = True
 
# auto download cover
# Automatically download the cover, if available
auto_download_cover = False
 
# enforce cpu limit
# Limit max simultaneous jobs to number of CPUs
enforce_cpu_limit = True
 
# gui layout
# The layout of the user interface. Wide has the Book details panel on the right and narrow has it at the bottom.
gui_layout = 'wide'
 
# show avg rating
# Show the average rating per item indication in the Tag browser
show_avg_rating = True
 
# disable animations
# Disable UI animations
disable_animations = True
 
# tag browser hidden categories
# tag browser categories not to display
tag_browser_hidden_categories = cPickle.loads('\x80\x02c__builtin__\nset\nq\x01]\x85Rq\x02.')
 


