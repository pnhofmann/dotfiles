// disable ghacks settings
// enable modern protocols
user_pref("network.dns.disableIPv6", false);
user_pref("network.http.spdy.enabled", true);
user_pref("network.http.spdy.enabled.deps", true);
user_pref("network.http.spdy.enabled.http2", true);
// no http unsecure
user_pref("security.insecure_connection_icon.enabled", false);
user_pref("security.insecure_connection_text.enabled", false);
user_pref("security.insecure_connection_icon.pbmode.enabled", false);
user_pref("security.insecure_connection_text.pbmode.enabled", false);
// disable blocklists
user_pref("extensions.blocklist.enabled", false);
user_pref("services.blocklist.update_enabled", false);
user_pref("services.blocklist.pinning.enabled", false);
// don't clear history
user_pref("privacy.clearOnShutdown.history", false);

// performance
user_pref("image.mem.max_decoded_image_kb", 3000);
user_pref("browser.download.animateNotifications", false);
user_pref("gfx.downloadable_fonts.enabled", true);
user_pref("layout.frame_rate.precise", true);
user_pref("browser.cache.memory.capacity", 3000);

// cache
user_pref("network.dnsCacheEntries", 10);
user_pref("network.dnsCacheExpiration", 10);
user_pref("network.cookie.lifetimePolicy", 2);

// enable reader
user_pref("reader.parse-on-load.enabled", true);

//unsigned addons
user_pref("xpinstall.signatures.required", false);

// don't do bullshit on first run
user_pref("distribution.openSUSE.bookmarksProcessed", true);
user_pref("browser.places.smartBookmarksVersion", 8);
user_pref("browser.startup.firstrunSkipsHomepage", false);
user_pref("browser.shell.didSkipDefaultBrowserCheckOnFirstRun", true);
// don't show annoying messages
user_pref("browser.tabs.closeWindowWithLastTab", false);
user_pref("browser.tabs.warnOnClose", false);
user_pref("browser.tabs.warnOnCloseOtherTabs", false);
user_pref("browser.tabs.warnOnOpen", false);
user_pref("general.warnOnAboutConfig", false);
user_pref("browser.shell.checkDefaultBrowser", false);
// don't use password manager
user_pref("signon.rememberSignons", false);

// sync server
user_pref("identity.sync.tokenserver.uri", "http://mozillasync.tux-server/token/1.0/sync/1.5");

// devtool ui customation
user_pref("devtools.toolbox.host", "side");
user_pref("devtools.toolbox.previousHost", "side");

// default download to "DOWNLOADS"
user_pref("browser.download.folderList", 1);

// dialog
user_pref("security.dialog_enable_delay", 500);

// disable drm
user_pref("browser.eme.ui.enabled", false);

// set theme
user_pref("lightweightThemes.selectedThemeID", "firefox-compact-dark@mozilla.org");
user_pref("lightweightThemes.persisted.footerURL", false);

// enable u2f
user_pref("security.webauth.u2f", true);

// hidpi
user_pref("layout.css.devPixelsPerPx", "1.5");

// audio
user_pref("media.cubeb.backend", "pulse-rust");
