export ZSH=~/.zsh/oh-my-zsh

# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="robbyrussell"
HYPHEN_INSENSITIVE="true"
DISABLE_AUTO_UPDATE="true"
DISABLE_AUTO_TITLE="true"
COMPLETION_WAITING_DOTS="true"
ZSH_CUSTOM=$HOME/.zsh/custom

#export LISTMAX=10000
alias reload='source $HOME/.zshrc'

plugins=(per-directory-history dirpersist last-working-dir zsh-syntax-highlighting-filetypes)
#autoload -U compinit && compinit
source $ZSH/oh-my-zsh.sh

# source env
for file in $(ls $HOME/.zsh/autoload); do
	source "$HOME/.zsh/autoload/$file"
done

cd $(pwd)
