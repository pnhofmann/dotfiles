#!/bin/sh
cd $(dirname $0)
WD=$(pwd)
for dir in $(find . -type d | grep -v '.git/'); do
    mkdir -p $HOME/$dir
done
for file in $(find . -type f | grep -v '.git/' | grep -v 'install.sh'); do
    ln -fs $WD/$file $HOME/$file
done
